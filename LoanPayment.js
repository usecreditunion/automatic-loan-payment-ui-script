/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*!
 * Ext JS Library 3.2.0
 * Copyright(c) 2006-2010 Ext JS, LLC
 * licensing@extjs.com
 * http://www.extjs.com/license
 */

/* global Ext */

//Working
console.clear();
Ext.ns('Ext.ux.grid');
//Ext.ns('app.grid');
    var dummyData = [];
    var person = [];
    var account = [];
    var share = [];
    var loan = [];
    var savingsTransfer = [];
    var loanTransfer = [];

    Ext.QuickTips.init();
    
    var centerPanel = new CR.FormPanel({
    region: 'center',
    autoScroll: true,
    frame: false,
    labelWidth: 150,
    bodyStyle: {
      padding: '5px',
      font: '12px arial,tahoma,helvetica,sans-serif'
    },
    listeners: {
      render: function() {
        AccountDetails();
      }
    }
    });
    
    CR.Core.viewPort = new Ext.Viewport({
        layout: 'border',
        autoScroll: true,
       items: [centerPanel ],
        listeners: {
            render: function() {               
            }
        }
    });

function AccountDetails (){
    var scope = this;
    var xml = new CR.XML();
    var sequence = xml.addContainer(xml.getRootElement(),'sequence');
    var transaction = xml.addContainer(sequence,'transaction');

    var step = xml.addContainer(transaction, 'step');
    var recordTree = xml.addContainer(step, 'recordTree');
    xml.addText(recordTree, 'tableName', 'PERSON');
    xml.addText(recordTree, 'viewName', 'RELATIONSHIP');
    xml.addText(recordTree, 'targetSerial', CR.Script.personSerial);
    xml.addOption(recordTree, 'includeRecordDetail', 'Y');
    var recordDetail = null;
    recordDetail = xml.addContainer(recordTree, 'recordDetail');
    xml.addText(recordDetail, 'tableName', 'PERSON');
    recordDetail = xml.addContainer(recordTree, 'recordDetail');
    xml.addText(recordDetail, 'tableName', 'ACCOUNT');
    recordDetail = xml.addContainer(recordTree, 'recordDetail');
    xml.addText(recordDetail, 'tableName', 'SHARE');
    recordDetail = xml.addContainer(recordTree, 'recordDetail');
    xml.addText(recordDetail, 'tableName', 'SH_TRANSFER');
    recordDetail = xml.addContainer(recordTree, 'recordDetail');
    xml.addText(recordDetail, 'tableName', 'LOAN');
        
    recordDetail = xml.addContainer(recordTree, 'recordDetail');
    xml.addText(recordDetail, 'tableName', 'LN_TRANSFER');
    CR.Core.ajaxRequest({
      url: 'DirectXMLPostJSON',
      xmlData: xml.getXMLDocument(),
      scope: scope,
      success: function(response) {
        var recordTreeResponse = null;
        var records = [];
        var tranResult = 'failed';
        var errorArray = [];
        var responseJson = CR.JSON.parse(response.responseText);
        var query = responseJson.query;
        if (query) {
          Ext.each(query.sequence, function(sequence) {
            Ext.each(sequence.transaction, function(transaction) {
              tranResult = transaction.$attr.result;
              Ext.each(transaction.exception, function(exception) {
                errorArray.push(exception.message);
              });
              Ext.each(transaction.step, function(step) {
                if (step.tranResult &&
                    step.tranResult.category &&
                    step.tranResult.category.option &&
                    step.tranResult.category.option === 'E') {
                  errorArray.push(step.tranResult.description);
                } else if (step.recordTree) {
                  recordTreeResponse=step.recordTree;
                } else if (step.record) {
                  records.push(step.record);
                }              
              });
            });
          });
        }
        if (tranResult !== 'posted' || errorArray.length > 0) {
            CR.Core.displayExceptions({ items: errorArray });
        } else {
            console.log('recordTreeResponse', recordTreeResponse);
            console.log('records', records);
            loanPaymentDisplay(records);
        }
      }
    });
};

function loanPaymentDisplay (records){

    var personDetails = []; 
    personDetails = records;   
    
    personDetails.forEach(function (entry){
       if(entry.tableName === 'PERSON'){
            person.push(entry);           
        }
        if(entry.tableName === 'ACCOUNT'){
            account.push(entry);
        }
        if(entry.tableName === 'SHARE'){
            share.push(entry);           
        }
        if(entry.tableName === 'SH_TRANSFER'){
            savingsTransfer.push(entry);          
        } 
        if(entry.tableName === 'LOAN'){
            loan.push(entry);          
        }        
        if(entry.tableName === 'LN_TRANSFER'){
            loanTransfer.push(entry);          
        }               
    });
    console.log('Loan: ' , loan);
      
   /* console.log('Person: ' , person);
    console.log('Account: ' , account);
    console.log('Share: ' , share);
    console.log('Loan: ' , loan);
    console.log('Savings Transfer', savingsTransfer);
    console.log('Loan Transfer', loanTransfer);*/
    
    var accountDetails = [];
    account.forEach(function(entry){
        accountDetails = entry; 

        var paymentDue, balance,creditLimit, paymentDueDate;
        //Loan Details
        if(accountDetails.field[21].newContents){ //verifying if Account has LoanAmount or not.                                        
            loan.forEach(function (entry){
                    // console.log('loan entry: '  ,entry);                           
                if(entry.field[57].newContents){
                        paymentDue = entry.field[57].newContents;
                } else {
                        paymentDue = '0.00';
                }
                if(entry.field[8].newContents){
                        balance = entry.field[8].newContents;
                } else {
                        balance = '0.00';
                }
                if(entry.field[12].newContents){
                        creditLimit = entry.field[12].newContents;
                } else {
                        creditLimit = '0.00';
                }
                  
                paymentDueDate =entry.field[77].newContents;
                
                function changeDateFormat(paymentDueDate){  // expects Y-m-d
                    var splitDate = paymentDueDate.split('-');
                    if(splitDate.count === 0){
                        return null;
                    }
                    var year = splitDate[0];
                    var month = splitDate[1];
                    var day = splitDate[2]; 
                    return month + '/' + day + '/' + year;
                }
                var newDate = changeDateFormat(paymentDueDate);
                
                // Verifying if there is close date on any loan accounts
                if(accountDetails.targetSerial === entry.targetParentSerialResult && !entry.field[175].newContents){ 
                    
                    var yesOrNo= 'No';
                    loanTransfer.forEach(function (test){
                   //console.log('loanTransfer' ,test);
                        if(test.targetParentSerialResult === entry.targetSerial && test.field[4].newContentsDescription === 'Automatic loan payment'  
                            && !test.field[37].newContents){
                            yesOrNo = 'Yes';                       
                        }
                        else if(test.targetParentSerialResult === entry.targetSerial && test.field[4].newContentsDescription === 'Automatic loan payment'  
                            && test.field[37].newContents){
                            yesOrNo = 'Automatic transfer Expired on '+test.field[37].newContents;
                        } 
                    });
                    
                    dummyData.push(
                        {paymentMethod: entry.field[55].newContentsDescription, accountNumber: 'Account Number: '+ accountDetails.rowDescription, targetSerial: entry.targetSerial,
                        targetParentSerial: accountDetails.targetSerial, lineOfCreditLoans: entry.rowDescription, paymentDue: paymentDue, creditLimit: creditLimit, balance: balance, 
                        due: newDate,verification: yesOrNo +'<br> Note: If Yes, the Payment Method should be selected as "Automatic transfer". <br> Not applicable for Loan IDs 7000 - 7009'}
                    ); 
                }            
            });
        }
    }); 
       
    var xg = Ext.grid;

    var reader = new Ext.data.JsonReader({
       // idProperty: 'accountNumber',
        fields: [
            {name: 'paymentMethod'},
            {name: 'accountNumber', type: 'string'},
            {name: 'targetSerial', type: 'string'},
            {name: 'targetParentSerial', type: 'string'},
            {name: 'lineOfCreditLoans', type: 'string'},
            {name: 'paymentDue', type: 'float'},
            {name: 'creditLimit', type: 'float'},
            {name: 'balance', type: 'float'},
            {name: 'due', type: 'date', dateFormat:'m/d/Y'},
            {name: 'verification' , type: 'string'}
        ]

    });
    var rowSelectedRecord;
    var grid = new xg.EditorGridPanel({
        ds: new Ext.data.GroupingStore({
            reader: reader,
            data: dummyData,
            sortInfo: {field: 'due', direction: 'ASC'},
            groupField: 'accountNumber'
        }),
        sm: new Ext.grid.RowSelectionModel({
                    singleSelect: true,
                    listeners: {
                        rowselect: function(sm, row, rec) {
                         //   console.log('rec', rec);
                            rowSelectedRecord = rec;
                          //  console.log('records', records);
                        }
                    }
                }),
        id: 'test',
        autoScroll: true,
        columns: [
            {
                id: 'lineOfCreditLoans',
                header: 'Line of Credit Loans',
                width: 60,
                sortable: true,
                dataIndex: 'lineOfCreditLoans',
                hideable: false
            },{
                id: 'paymentMethod',
                name: 'paymentMethod',
                header: 'Payment Method',
                width: 60,
               dataIndex: 'paymentMethod',
                hideable: false,              
                editor: {
                    xtype: 'crOptionField',
                    crColumnDescription: 'Option Field',
                    crColumnName: 'myOptionFiled',
                    crOptions: [
                        ["Cash","Cash"],
                        ["Cash with coupons", "Cash with coupons"],
                        ["Automatic transfer", "Automatic transfer"],
                        ["ACH", "ACH"],
                        ["Payroll", "Payroll"],
                        ["Distribution","Distribution"]
                        ]}
            },{
                header: 'Account Number',
                width: 20,
                sortable: true,
                dataIndex: 'accountNumber'
            },{
                header: 'Due Date',
                width: 25,
                sortable: true,
                dataIndex: 'due',
                renderer: Ext.util.Format.dateRenderer('m/d/Y')
            },{
                header: 'Payment Due',
                width: 20,
                sortable: true,
                dataIndex: 'paymentDue',
                renderer : Ext.util.Format.usMoney
            },{
                header: 'Credit Limit/Approved Amount',
                width: 20,
                sortable: true,
                renderer: Ext.util.Format.usMoney,
                dataIndex: 'creditLimit'
            },{
                header: 'Balance',
                width: 20,
                sortable: false,
                groupable: false,
                renderer: Ext.util.Format.usMoney,
                dataIndex: 'balance',
                summaryRenderer: Ext.util.Format.usMoney
            },{
                header: 'Has Automatic transfer Record',
                 dataIndex: 'verification'
            }//end
        ],
        view: new Ext.grid.GroupingView({
            forceFit: true,
            showGroupName: false,
            enableNoGroups: false,
			enableGroupingMenu: false,
            hideGroupedColumn: true
        }),
        frame: true,
        width: 1200,
        height: 500,
        clicksToEdit: 1,
        collapsible: true,
        animCollapse: false,
        trackMouseOver: false,
        title: 'Loan Payment',
        iconCls: 'icon-grid',
         bbar: [{
            text: 'Payment Method',
            handler : function(){          
                if(grid.lastActiveEditor){
                    var lastActiveEditor = grid.lastActiveEditor.field;                  
                    var lineOfCreditLoans = rowSelectedRecord.data.lineOfCreditLoans;
                    var res = lineOfCreditLoans.substr(13, 14);
                    var patt1 = /[7]/g;
                    var result = res.match(patt1) || 'notSeven' ;                              
                    if(result !== 'notSeven' && rowSelectedRecord.data.paymentMethod === 'Automatic transfer'){
                        Ext.Msg.alert('Loan ID from 7000 - 7009', 'Payment Method cannot be selected as \'Automatic transfer\'. <br> Call Loan Servicing - xx 6555'); 
                    }
                    else{
                        updatePaymentMethod(lastActiveEditor,rowSelectedRecord);
                    }
                } else{
                    Ext.Msg.alert('Payment Method', 'No change in Payment Method!');
                }
            }
        }, { 
                text: 'Insert Automatic Transfer Method',
                handler : function(){
                    var rowselected = rowSelectedRecord;
                    if(rowselected){
                        var lineOfCreditLoans = rowselected.data.lineOfCreditLoans;
                        var res = lineOfCreditLoans.substr(13, 14);
                        var patt1 = /[7]/g;
                        var result = res.match(patt1) || 'notSeven' ;                              
                        if(result !== 'notSeven'){
                            Ext.Msg.alert('Loan ID from 7000 - 7009', 'Automatic Transfer Loan Payment Method cannot be added for Loan ID between 7000 - 7009. <br> Call Loan Servicing - xx 6555'); 
                        } else{
                            insertAutomaticTransfer(rowselected);
                        }
                    }else{
                        Ext.Msg.alert('Loan Selection', 'Please select a Loan');
                    }                    
                }
            }]
    });    
     
centerPanel.add(grid);
centerPanel.doLayout();

};

function updatePaymentMethod(lastActiveEditor,rowSelectedRecord){
       
    var updatePaymentMethod = [];    var paymentMethod;
     
    updatePaymentMethod = rowSelectedRecord.data;
                           
    console.log('Updated data : ', updatePaymentMethod);
    if(updatePaymentMethod.paymentMethod === 'Cash'){
       paymentMethod = 'C';
    } else if(updatePaymentMethod.paymentMethod === 'Cash with coupons'){
       paymentMethod = 'O';  
    } else if(updatePaymentMethod.paymentMethod === 'Automatic transfer'){
       paymentMethod = 'T';  
    } else if(updatePaymentMethod.paymentMethod === 'ACH'){
       paymentMethod = 'A';  
    } else if(updatePaymentMethod.paymentMethod === 'Payroll'){
       paymentMethod = 'P';  
    } else if(updatePaymentMethod.paymentMethod === 'Distribution'){
       paymentMethod = 'D';  
    }
      
    var xml = new CR.XML();
    var sequence = xml.addContainer(xml.getRootElement(),'sequence');
    var transaction = xml.addContainer(sequence,'transaction');

    var step = xml.addContainer(transaction, 'step');
    var record = xml.addContainer(step, 'record');
    xml.setAttribute(record, 'label', 'LOAN_record');
    xml.addText(record, 'tableName', 'LOAN');
    xml.addOption(record, 'operation', 'U');
    xml.addText(record, 'targetSerial', updatePaymentMethod.targetSerial);
    xml.addOption(record, 'includeRowDescriptions', 'Y');

    var field = null;
    field = xml.addContainer(record, 'field');
    xml.addText(field, 'columnName', 'PAYMENT_METHOD');
    xml.addOption(field, 'operation', 'S');
    xml.addText(field, 'newContents', paymentMethod);   

    CR.Core.ajaxRequest({
        url: 'DirectXMLPostJSON',
        xmlData: xml.getXMLDocument(),
        success: function(response) {
        var recordResponse = null;
        var tranResult = 'failed';
        var errorArray = [];
        var responseJson = CR.JSON.parse(response.responseText);
        var query = responseJson.query;
        if (query) {
          Ext.each(query.sequence, function(sequence) {
            Ext.each(sequence.transaction, function(transaction) {
              tranResult = transaction.$attr.result;
              Ext.each(transaction.exception, function(exception) {
                errorArray.push(exception.message);
              });
              Ext.each(transaction.step, function(step) {
                if (step.tranResult &&
                    step.tranResult.category &&
                    step.tranResult.category.option &&
                    step.tranResult.category.option === 'E') {
                  errorArray.push(step.tranResult.description);
                } else if (step.record &&
                           step.record.$attr &&
                           step.record.$attr.label === 'LOAN_record') {
                  recordResponse = step.record;
                }
              });
            });
          });
        }
        if (tranResult !== 'posted' || errorArray.length > 0) {
          CR.Core.displayExceptions({ items: errorArray });
        } else {
          console.log('recordResponse', recordResponse);
          Ext.Msg.alert('Payment Method', 'Loan Payment method updated to ' +updatePaymentMethod.paymentMethod);
        }
      }
    });        
}

function insertAutomaticTransfer(rowselected){
    
    var myFormPanel = new CR.FormPanel({
    labelWidth: 175,
    id: 'formwidgets',
    frame: true,
    title: 'Insert Loan Transfer',
    bodyStyle:'padding:5px 5px 0',
    width: 1200,
    defaults: {
      width: 230
    },
    items: [
        {
            xtype: 'crOptionField',
            crColumnDescription: 'Category',
            crColumnName: 'category',
            disabled: true,
            id:'categoryId',
            crOptions: [
                ["P","Automatic loan payment"],
                ["S","Scheduled disbursement"],
                ["I","Escrow disbursement"]
            ]
        },{
            xtype: 'crOptionField',
            crColumnDescription: 'Method',
            crColumnName: 'method',
            id: 'methodId',
            disabled: true,
            crOptions: [
                ["S","Savings transfer"],
                ["G","General ledger"],
                ["C","Credit union check"]
            ]
        },{
            xtype: 'crSerialField',
            id: 'savingsId',
            crColumnDescription: 'Savings',
            crTableName: 'SHARE',
            crNullAllowed: true
        },{
            xtype: 'crMoneyField',
            crColumnDescription: 'Amount',
            crColumnName: 'amount',
            id:'amountId'
        },{
            xtype: 'crOptionField',
            crColumnDescription: 'Amount Override',
            crColumnName: 'amountOverride',
            id:'amountOverrideId',
            crOptions: [	
                ["-","None"],
                ["P","Loan payment"],
                ["p","Loan payment if it exceeds amount"],
                ["Q","25% of Loan payment"],
                ["H","50% of Loan payment"],
                ["D","Loan due amount"],
                ["d","Loan due amount if it exceeds amount"],
                ["B","Loan cycle balance"],
                ["b","Loan cycle balance if it exceeds amount"],
                ["A","Available balance"],
                ["S","Available balance in excess of amount"]
            ]
        },{          
            xtype: 'crDateField',
            crColumnDescription: '*Effective Date', 
            crColumnName: 'myDateFiled',
            id: 'myDateFiledId',
            blank: true,
            textAlert : 'Hello'       
        },
        {
            xtype:'textarea',
            fieldLabel: 'Important Note for Effective Date',
            value : 'Effective Date is not Necessary unless starting for Future Date. Only Use if draft on Payment Due Date.\n\
It is not compulsary to add Effective Date.\n\
\n\
Call Loan Servicing - xx 6555',
            disabled: true,
            width : 600,
            height : 70
        }
    ],
    buttons: [{
        text: 'Insert',
        listeners: {
            click: function(button, event) {
                insertLoanTransfer(rowselected);
            }
        }
    },  {
        text: 'Reset',
        handler: function() {
            myFormPanel.getForm().reset();
        }
    }],
    buttonAlign: 'left'
    });
    centerPanel.add(myFormPanel);    
    var id = Ext.get('formwidgets');     
    if(id !== null) {
        id.remove(); 
    }
    centerPanel.doLayout();      
}


function insertLoanTransfer(rowselected){
    console.log('rowselected',rowselected);
    var targetSerial = rowselected.data.targetSerial;
    var loanDetails = rowselected.data.lineOfCreditLoans;
    var shareSerial;
    var savings = Ext.get('savingsId').getValue();
    var amount = Ext.get('amountId').getValue();
    var amountOverride = Ext.get('amountOverrideId').getValue();
    
    var effectiveDate = Ext.get('myDateFiledId').getValue();
    function changeDateFormat(paymentDueDate){  // expects Y-m-d m/d/Y 01/17/2018
        var splitDate = paymentDueDate.split('/');
        if(splitDate.count === 0){
            return null;
        }
        var year = splitDate[2];
        var month = splitDate[0];
        var day = splitDate[1]; 
        return year + '-' + month + '-' + day;
    }
    var effectiveDateCovertedValue = changeDateFormat(effectiveDate);
    console.log('effectiveDateCovertedValue', effectiveDateCovertedValue);

    share.forEach(function (entry){     
        if(savings === entry.rowDescription) {
           shareSerial = entry.targetSerial;          
        }
    });
    
    if(CR.MoneyField.convertFromDisplay(amount) > 0.00 || amountOverride === 'Loan due amount if it exceeds amount'){
        amountOverride = 'd';
    } else if(amountOverride === 'None'){
        amountOverride = '-';
    } else if(amountOverride === 'Loan payment'){
        amountOverride = 'P';
    } else if(amountOverride === 'Loan payment if it exceeds amount'){
        amountOverride = 'p'; 
    } else if(amountOverride === '25% of Loan payment'){
        amountOverride = 'Q';
    } else if(amountOverride === '50% of Loan payment'){
        amountOverride = 'H';
    } else if(amountOverride === 'Loan due amount'){
        amountOverride = 'D';
    } else if(amountOverride === 'Loan cycle balance'){
        amountOverride = 'B';
    } else if(amountOverride === 'Loan cycle balance if it exceeds amount'){
        amountOverride = 'b';
    } else if(amountOverride === 'Available balance'){
        amountOverride = 'A';
    } else if(amountOverride === 'Available balance in excess of amount'){
        amountOverride = 'S'; 
    }        
    
    var xml = new CR.XML();
    var sequence = xml.addContainer(xml.getRootElement(),'sequence');
    var transaction = xml.addContainer(sequence,'transaction');

    var step = xml.addContainer(transaction, 'step');
    var record = xml.addContainer(step, 'record');
    xml.setAttribute(record, 'label', 'LN_TRANSFER_record');
    xml.addText(record, 'tableName', 'LN_TRANSFER');
    xml.addOption(record, 'operation', 'I');
    xml.addText(record, 'targetParentSerial', targetSerial);
    xml.addOption(record, 'includeTableMetadata', 'N');
    xml.addOption(record, 'includeColumnMetadata', 'N');
    xml.addOption(record, 'includeRowDescriptions', 'Y');
    xml.addOption(record, 'includeAllColumns', 'N');

    var field = null;
    field = xml.addContainer(record, 'field');
    xml.addText(field, 'columnName', 'CATEGORY');
    xml.addOption(field, 'operation', 'S');
    xml.addText(field, 'newContents', 'P');

    field = xml.addContainer(record, 'field');
    xml.addText(field, 'columnName', 'METHOD');
    xml.addOption(field, 'operation', 'S');
    xml.addText(field, 'newContents', 'S');

    field = xml.addContainer(record, 'field');
    xml.addText(field, 'columnName', 'SHARE_SERIAL');
    xml.addOption(field, 'operation', 'S');
    xml.addText(field, 'newContents', shareSerial);

    field = xml.addContainer(record, 'field');
    xml.addText(field, 'columnName', 'AMOUNT');
    xml.addOption(field, 'operation', 'S');
    xml.addText(field, 'newContents', CR.MoneyField.convertFromDisplay(amount));

    field = xml.addContainer(record, 'field');
    xml.addText(field, 'columnName', 'AMOUNT_OVERRIDE');
    xml.addOption(field, 'operation', 'S');
    xml.addText(field, 'newContents', amountOverride);
    
    if(!effectiveDateCovertedValue){
        field = xml.addContainer(record, 'field');
        xml.addText(field, 'columnName', 'EFFECTIVE_DATE');
        xml.addOption(field, 'operation', 'S');
        xml.addText(field, 'newContents', effectiveDateCovertedValue);
    }

    CR.Core.ajaxRequest({
      url: 'DirectXMLPostJSON',
      xmlData: xml.getXMLDocument(),
      success: function(response) {
        var recordResponse = null;
        var tranResult = 'failed';
        var errorArray = [];
        var responseJson = CR.JSON.parse(response.responseText);
        var query = responseJson.query;
        if (query) {
          Ext.each(query.sequence, function(sequence) {
            Ext.each(sequence.transaction, function(transaction) {
              tranResult = transaction.$attr.result;
              Ext.each(transaction.exception, function(exception) {
                errorArray.push(exception.message);
              });
              Ext.each(transaction.step, function(step) {
                if (step.tranResult &&
                    step.tranResult.category &&
                    step.tranResult.category.option &&
                    step.tranResult.category.option === 'E') {
                  errorArray.push(step.tranResult.description);
                } else if (step.record &&
                           step.record.$attr &&
                           step.record.$attr.label === 'LN_TRANSFER_record') {
                  recordResponse = step.record;
                }
              });
            });
          });
        }
        if (tranResult !== 'posted' || errorArray.length > 0) {
          CR.Core.displayExceptions({ items: errorArray });
        } else {
          console.log('recordResponse', recordResponse);
          Ext.Msg.alert('Automatic Loan Transfer', 'Automatic Loan Trasfer is added for the Loan: <b>'+loanDetails +'</b>');
        }
      }
    });   
       
    updatePaymentMethodForNewInsert(targetSerial);
   
}



function updatePaymentMethodForNewInsert(targetSerial){
    
    var xml = new CR.XML();
var sequence = xml.addContainer(xml.getRootElement(),'sequence');
var transaction = xml.addContainer(sequence,'transaction');

var step = xml.addContainer(transaction, 'step');
var record = xml.addContainer(step, 'record');
xml.setAttribute(record, 'label', 'LOAN_record');
xml.addText(record, 'tableName', 'LOAN');
xml.addOption(record, 'operation', 'U');
xml.addText(record, 'targetSerial', targetSerial);
xml.addOption(record, 'includeTableMetadata', 'N');
xml.addOption(record, 'includeColumnMetadata', 'N');
xml.addOption(record, 'includeRowDescriptions', 'Y');
xml.addOption(record, 'includeAllColumns', 'N');

var field = null;
field = xml.addContainer(record, 'field');
xml.addText(field, 'columnName', 'PAYMENT_METHOD');
xml.addOption(field, 'operation', 'S');
xml.addText(field, 'newContents', 'T');    

CR.Core.ajaxRequest({
  url: 'DirectXMLPostJSON',
  xmlData: xml.getXMLDocument(),
  success: function(response) {
    var recordResponse = null;
    var tranResult = 'failed';
    var errorArray = [];
    var responseJson = CR.JSON.parse(response.responseText);
    var query = responseJson.query;
    if (query) {
      Ext.each(query.sequence, function(sequence) {
        Ext.each(sequence.transaction, function(transaction) {
          tranResult = transaction.$attr.result;
          Ext.each(transaction.exception, function(exception) {
            errorArray.push(exception.message);
          });
          Ext.each(transaction.step, function(step) {
            if (step.tranResult &&
                step.tranResult.category &&
                step.tranResult.category.option &&
                step.tranResult.category.option === 'E') {
              errorArray.push(step.tranResult.description);
            } else if (step.record &&
                       step.record.$attr &&
                       step.record.$attr.label === 'LOAN_record') {
              recordResponse = step.record;
            }
          });
        });
      });
    }
    if (tranResult !== 'posted' || errorArray.length > 0) {
      CR.Core.displayExceptions({ items: errorArray });
    } else {
      console.log('recordResponse', recordResponse);
    }
  }
});

}